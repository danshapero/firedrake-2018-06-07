{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import firedrake\n",
    "from firedrake import sym, inner, grad, div, dx, ds, \\\n",
    "    MixedVectorSpaceBasis, VectorSpaceBasis, DirichletBC"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def ɛ(u):\n",
    "    return sym(grad(u))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 64\n",
    "mesh = firedrake.UnitSquareMesh(64, 64)\n",
    "Q = firedrake.FunctionSpace(mesh, 'CG', 1)\n",
    "V = firedrake.VectorFunctionSpace(mesh, 'CG', 2)\n",
    "Z = V * Q"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "μ = firedrake.Constant(1.0)  # viscosity coefficient"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters = {\n",
    "    'mat_type': 'matfree',\n",
    "    'ksp_type': 'gmres',\n",
    "    'pc_type': 'fieldsplit',\n",
    "    'pc_fieldsplit_type': 'schur',\n",
    "    'pc_fieldsplit_schur_fact_type': 'diag',\n",
    "    'fieldsplit_0_ksp_type': 'preonly',\n",
    "    'fieldsplit_0_pc_type': 'python',\n",
    "    'fieldsplit_0_pc_python_type': 'firedrake.AssembledPC',\n",
    "    'fieldsplit_0_assembled_pc_type': 'hypre',\n",
    "    'fieldsplit_1_ksp_type': 'preonly',\n",
    "    'fieldsplit_1_pc_type': 'python',\n",
    "    'fieldsplit_1_pc_python_type': 'firedrake.MassInvPC',\n",
    "    'fieldsplit_1_Mp_ksp_type': 'preonly',\n",
    "    'fieldsplit_1_Mp_pc_type': 'ilu'\n",
    "}\n",
    "\n",
    "nullspace = MixedVectorSpaceBasis(Z, [Z.sub(0), VectorSpaceBasis(constant=True)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Traditional method\n",
    "\n",
    "Since our domain is a square, we can get away with directly imposing the Dirichlet part of the friction BCs by setting a component of the velocity field to 0.\n",
    "For arbitrary domains, we don't have this option."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def solve_dirichlet(κ, u_κ):\n",
    "    z = firedrake.Function(Z)\n",
    "    u, p = firedrake.split(z)\n",
    "\n",
    "    n = firedrake.FacetNormal(mesh)\n",
    "    u_n = inner(u, n)\n",
    "    Pu = u - u_n * n\n",
    "    \n",
    "    J_viscous = μ * inner(ɛ(u), ɛ(u)) * dx\n",
    "    J_pressure = p * div(u) * dx\n",
    "    J_friction = 0.5 * κ * inner(Pu - u_κ, Pu - u_κ) * ds((4,)) \\\n",
    "                + 0.5 * κ * inner(Pu, Pu) * ds((1, 2, 3))\n",
    "    J = J_viscous - J_pressure + J_friction\n",
    "\n",
    "    bc_x = DirichletBC(Z.sub(0).sub(0), firedrake.Constant(0), (1, 2))\n",
    "    bc_y = DirichletBC(Z.sub(0).sub(1), firedrake.Constant(0), (3, 4))\n",
    "\n",
    "    firedrake.solve(firedrake.derivative(J, z) == 0, z, bcs=[bc_x, bc_y],\n",
    "                    nullspace=nullspace,\n",
    "                    solver_parameters=parameters)\n",
    "    return z.split()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Nitsche method\n",
    "\n",
    "We're using the P2-P1 velocity-pressure pair for the Stokes equations.\n",
    "Consequently the derivative of the velocity is is linear in each triangle, so we'll take the degree to be 1 for the constant in the Warburton-Hesthaven theorem.\n",
    "On the other hand we have `d**2` components of the stress tensor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mesh_util import minimum_angle\n",
    "θ = minimum_angle(mesh)\n",
    "k, d = 2, 2\n",
    "η = 2 * d * k * (k + d - 1) / np.cos(θ) / np.tan(θ/2)\n",
    "\n",
    "def solve_nitsche(κ, u_κ):\n",
    "    z = firedrake.Function(Z)\n",
    "    u, p = firedrake.split(z)\n",
    "\n",
    "    h = firedrake.CellSize(mesh)\n",
    "    n = firedrake.FacetNormal(mesh)\n",
    "    u_n = inner(u, n)\n",
    "    Pu = u - u_n * n\n",
    "\n",
    "    I = firedrake.Identity(2)\n",
    "    σ_n = inner(n, (2 * μ * ɛ(u) - p * I) * n)\n",
    "\n",
    "    J_viscous = μ * inner(ɛ(u), ɛ(u)) * dx\n",
    "    J_pressure = p * div(u) * dx\n",
    "    J_friction = 0.5 * κ * inner(Pu - u_κ, Pu - u_κ) * ds((4,)) \\\n",
    "                + 0.5 * κ * inner(Pu, Pu) * ds((1, 2, 3))\n",
    "    J_lagrange = σ_n * u_n * ds\n",
    "    J_penalty = 0.5 * η * μ / h * u_n**2 * ds\n",
    "    J = J_viscous - J_pressure + J_friction - J_lagrange + J_penalty\n",
    "    \n",
    "    firedrake.solve(firedrake.derivative(J, z) == 0, z,\n",
    "                    nullspace=nullspace,\n",
    "                    solver_parameters=parameters)\n",
    "    return z.split()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "κ = firedrake.Constant(10.0)\n",
    "u_κ = firedrake.as_vector((0.5, 0.0))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u_dirichlet, p_dirichlet = solve_dirichlet(κ, u_κ)\n",
    "u_nitsche, p_nitsche = solve_nitsche(κ, u_κ)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firedrake.norm(u_dirichlet - u_nitsche)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "δu = u_dirichlet - u_nitsche\n",
    "firedrake.plot(firedrake.interpolate(firedrake.sqrt(inner(δu, δu)), Q))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fancy plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xs = np.linspace(0, 1, N + 1)\n",
    "ys = np.linspace(0, 1, N + 1)\n",
    "\n",
    "U = np.zeros((2, N+1, N+1))\n",
    "for j, x in enumerate(xs):\n",
    "    for i, y in enumerate(ys):\n",
    "        U[:, i, j] = u_nitsche.at((x, y), tolerance=1e-12)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "speed = np.sqrt(U[0,:,:]**2 + U[1,:,:]**2)\n",
    "fig, ax = plt.subplots()\n",
    "ax.set_aspect('equal')\n",
    "ax.streamplot(xs, ys, U[0,:,:], U[1,:,:], color=speed, density=1.2, cmap='magma')\n",
    "plt.show(fig)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.set_aspect('equal')\n",
    "ctr = ax.contourf(xs, ys, speed, cmap='magma')\n",
    "fig.colorbar(ctr)\n",
    "plt.show(fig)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "firedrake",
   "language": "python",
   "name": "firedrake"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
